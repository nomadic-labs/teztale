.DEFAULT_GOAL := all

.PHONY: all clean

all:
	dune build

clean:
	dune clean
